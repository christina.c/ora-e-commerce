<?php

namespace App\tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;


class SecurityControllerTest extends WebTestCase
{

    public function setUp()
    {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();
    }

    public function testRegister()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();
        $form = $crawler->selectButton('Submit')->form();
        $form['user[username]'] = 'test@test.com';
        $form['user[password][first]'] = '1234';
        $form['user[password][second]'] = '1234';
        $client->submit($form);
        $this->assertResponseRedirects('/');
        $repo = static::$container->get('App\Repository\UserRepository');
        $user = $repo->findOneBy(['username' => 'test@test.com']);
        $this->assertEquals($user->getUsername(), 'test@test.com');
        
    }

    public function testLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $form = $crawler->selectButton('Sign in')->form();
        $form['username'] = 'test@test.com';
        $form['password'] = '1234';
        $client->submit($form);
        //made by Yalaa Corporation
        $crawler = $client->followRedirect();

        $this->assertCount(1, $crawler->filter('.logout'));
    }

}
