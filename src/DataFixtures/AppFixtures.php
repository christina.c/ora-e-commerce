<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $product1 = new Product();
        $product1->setColor(['black'])->setName('Corvus')->setPrice(275)->setSize([38, 39, 40, 41])->setImagePath('/uploads/33019_wide_1__1_.jpg');
        $product2 = new Product();
        $product2->setColor(['black'])->setName('Cassiopeia')->setPrice(190)->setSize([36,37, 38, 40])->setImagePath('https://gitlab.com/nerses/ora-e-commerce/uploads/dae18229caf1f8bbaca5b62bb047bc23/33019_5_1__1_.jpg');
        $product3 = new Product();
        $product3->setColor(['black'])->setName('Andromeda')->setPrice(99)->setSize([39, 40, 41, 45])->setImagePath('https://gitlab.com/nerses/ora-e-commerce/uploads/c5c7c7539938220c22acc0d78f6e26af/29035_4__1_.jpg');
        $product4 = new Product();
        $product4->setColor(['black'])->setName('Lyra')->setPrice(100)->setSize([36,38,40, 41])->setImagePath('https://gitlab.com/nerses/ora-e-commerce/uploads/45269e1c434e5194323a3965c8d5ccac/29035_5.png');

           
        $manager->persist($product1);
        $manager->persist($product2);
        $manager->persist($product3);
        $manager->persist($product4);
        
        $manager->flush();
        
        for ($i=1; $i < 5; $i++) { 
    
                $user = new User();
                $hashedPassword = $this->encoder->encodePassword($user, '1234');
    
                $user->setUsername('User')
                ->setPassword($hashedPassword)
                ->setRoles(['ROLE_USER']);
             }


        $manager->persist($user);

    
        
        $user = new User();
        
        $hashedPassword = $this->encoder->encodePassword($user, 'admin');

        $user->setUsername('admin')
        ->setPassword($hashedPassword)
        ->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);


        $manager->flush();

    
    }

       


    // for ($i = 0; $i < 20; $i++) {

    //     $product = new Product();
    //     $product->setName("Test");

    //     $manager->persist($product);
    // }

}
