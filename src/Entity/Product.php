<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="array")
     */
    private $size = [];

    /**
     * @ORM\Column(type="array")
     */
    private $color = [];

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column (type="string")
     */
    private $imagePath;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineCart", mappedBy="products")
     */
    private $linecart;

    public function __construct()
    {
        $this->linecart = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSize(): ?array
    {
        return $this->size;
    }

    public function setSize(array $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getColor(): ?array
    {
        return $this->color;
    }

    public function setColor(array $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * @return Collection|LineCart[]
     */
    public function getLinecart(): Collection
    {
        return $this->linecart;
    }

    public function addLinecart(LineCart $linecart): self
    {
        if (!$this->linecart->contains($linecart)) {
            $this->linecart[] = $linecart;
            $linecart->setProducts($this);
        }

        return $this;
    }

    public function removeLinecart(LineCart $linecart): self
    {
        if ($this->linecart->contains($linecart)) {
            $this->linecart->removeElement($linecart);
            // set the owning side to null (unless already changed)
            if ($linecart->getProducts() === $this) {
                $linecart->setProducts(null);
            }
        }

        return $this;
    }

}
