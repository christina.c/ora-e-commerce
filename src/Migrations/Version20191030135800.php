<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191030135800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE line_cart (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, products_id INT DEFAULT NULL, quantity INT DEFAULT NULL, size INT NOT NULL, price DOUBLE PRECISION NOT NULL, color VARCHAR(255) NOT NULL, INDEX IDX_80EE7B6EA76ED395 (user_id), INDEX IDX_80EE7B6E6C8A81A9 (products_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, price DOUBLE PRECISION NOT NULL, size LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', color LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', name VARCHAR(255) NOT NULL, image_path VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE line_cart ADD CONSTRAINT FK_80EE7B6EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE line_cart ADD CONSTRAINT FK_80EE7B6E6C8A81A9 FOREIGN KEY (products_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE line_cart DROP FOREIGN KEY FK_80EE7B6EA76ED395');
        $this->addSql('ALTER TABLE line_cart DROP FOREIGN KEY FK_80EE7B6E6C8A81A9');
        $this->addSql('DROP TABLE line_cart');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE product');
    }
}
