<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price')
            ->add('size', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices'  => [
                    '36' =>36,
                    '37' =>37,
                    '38' =>38,
                    '39' =>39,
                    '40' =>40,
                    '41' =>41,
                    '42' =>42,
                    '43' =>43,
                    '44' =>44,
                    '45' =>45,
                    '46' =>46,
                ],
            ])
  
            ->add('color')
            ->add('name')
            ->add('imagePath')
        ;
    }













    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
