<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;



class ProductController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/products", name="products_all")
     */
    public function seeAllProducts(ProductRepository $productsRepo, Request $request)
    {
        $filter = $request->get('filter');
        $order = ['name' => 'ASC'];
        if ($filter == 'Price growing') {
            $order = ['price' => 'ASC'];
        } elseif ($filter == 'Price decreasing') {
            $order = ['price' => 'DESC'];
        }
        return $this->render('product/index.html.twig', [
            'products_twig' => $productsRepo->findBy([], $order),
        ]);
    }

    /**
     * @Route("/add-article", name="add_article")
     */
    public function addArticle(Request $request, ObjectManager $manager)
    {
        $post = new Product();
        $form = $this->createForm(ProductType::class, $post);
        $form->handleRequest($request);
        $showText = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($post);
            $manager->flush();
            $showText = true;
        }
        return $this->render('product/add-article.html.twig', [
            'form' => $form->createView(),
            'showText' => $showText
        ]);
    }
}
