<?php

namespace App\Controller;

use App\Entity\LineCart;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class CartController extends AbstractController
{
    /**
     * @Route ("/article/delete/{id}", name="delete_article")
     */
    public function deleteArticle(ObjectManager $manager, Product $product)
    {
        $manager->remove($product);
        $manager->flush();
        return $this->redirectToRoute('products_all');
    }

    /**
     * @Route("/user/lineCart/{id}", name="line_cart")
     */
    public function lineCart(Product $product, ObjectManager $manager)
    {

        $lineCart = new LineCart();
        $lineCart ->setProducts($product)
            ->setColor('black')
            ->setPrice($product->getPrice())
            ->setQuantity(1)
            ->setSize(39);

        $manager->persist($lineCart);

        $this->getUser()->addLineCart($lineCart);

        $manager->flush();

        return $this->redirectToRoute('products_all');
    }


    /**
     * @Route("/remove/product/{id}", name="remove_product")
     */
    public function RemoveProductLineCart(ObjectManager $manager, LineCart $lineCart)

    {
        $this->getUser()->removeLineCart($lineCart);

        $manager->flush();

        return $this->redirectToRoute('show_cart');
    }

    /**
     * @Route("show-cart", name="show_cart")
     */
    public function showCart()

    {
        $total = 0;

        // todo $this->getUser() NULL ou vide alors redirect to login
        $cart = $this->getUser()->getLineCart();

        foreach ($cart as $line) {
            $total += $line->getProducts()->getPrice();
        }

        return $this->render('line_cart/index.html.twig', [
            "total" => $total
        ]);
    }
    /**
     * @Route("/form-cart", name="form_cart")
     */
    public function buy(){
        return $this->render('line_cart/form_paiement.html.twig');
    }
    /**
     * @Route("validation", name="validation")
     */
    public function validation() {
        return $this->render('line_cart/Validation.html.twig');
    }
}
