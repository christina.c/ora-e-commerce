# ORA e-Commerce

### Introduction

Ce projet est un e-commerce par groupe de 4. Nous devons créer une interface interactive entre les Users et le prestataire. 

Le thème de notre e-commerce est la vente de basket, nous avons crée notre marque qui s'appelle ORA.

Dans ce projet, nous avons réparti les tâches et le code entre les 4 personnes du groupe.



###  User Stories

- En tant qu'utilisateur.ice, je veux pouvoir visualiser la liste des produits filtrés pour faciliter mon choix
- En tant qu'utilisateur.ice, je veux pouvoir ajouter des produits à mon panier pour passer rapidement ma commande
- En tant que visiteur.euse, je veux pouvoir créer mon compte pour accéder à des fonctionnalités supplémentaires du site
- En tant qu'administrateur.ice, je veux pouvoir gérer les produits disponibles afin de modifier le catalogue Rappel : vous pouvez découper les user story (culture du backlog)
- En tant que utilisateur.ice je veux pouvoir modifier des produits sur mon panier afin de mettre à jour ma commande
- En tant que utilisateur.ice je souhaiterai contacter un service après vente afin de régler des problèmes et avoir des infos sur les produits
- En tant que utilisateur.ice je veux pouvoir supprimer ma commande afin de revenir sur mes choix
- En tant que client je veux pouvoir suivre ORA sur les réseaux sociaux afin de voir leur actualité



### Maquette

#### Mobile 

![mobile](./maquettes/mobile.png)

![mobile2](./maquettes/mobile2.jpg)



![mobile3](./maquettes/mobile3.png)





#### Ordinateur

![ordinateur1](./maquettes/ordinateur1.jpg)



![ordit2](./maquettes/ordit2.jpg)



![ordinateur3](./maquettes/ordinateur3.jpg)





###  Diagramme UML

####  Use Case

![Guest_UseCaseDiagram](./maquettes/UML/Guest_UseCaseDiagram.jpg)



![User_UseCaseDiagram](./maquettes/UML/User_UseCaseDiagram.jpg)





####  Class

![ClassDiagram_E_Commerce](./maquettes/UML/ClassDiagram_E_Commerce.jpg)



###  Code 

Voici quelque bout de code.



```php
<?php



namespace App\DataFixtures;



use App\Entity\*Product*;

use Doctrine\Bundle\FixturesBundle\*Fixture*;

use Doctrine\Common\Persistence\*ObjectManager*;



*class* AppFixtures extends *Fixture*

{

​    public *function* load(*ObjectManager* $manager)

​    {

​        $product1 = new *Product*;

​        $product1->setColor(['black'])->setName('Corvus')->setPrice(275)->setSize([38])->setImagePath('https://cdn.pixabay.com/photo/2016/11/21/15/54/countryside-1846093_960_720.jpg');

​        $product2 = new *Product*;

​        $product2->setColor(['white'])->setName('Cassiopeia')->setPrice(190)->setSize([40])->setImagePath('https://cdn.pixabay.com/photo/2017/07/30/15/49/adidas-2554690_960_720.jpg');

​        $product3 = new *Product*;

​        $product3->setColor(['grey'])->setName('Andromeda')->setPrice(99)->setSize([45])->setImagePath('https://cdn.pixabay.com/photo/2016/04/12/14/08/shoe-1324431_960_720.jpg');

​        $product4 = new *Product*;

​        $product4->setColor(['multi'])->setName('Lyra')->setPrice(100)->setSize([36])->setImagePath('https://cdn.pixabay.com/photo/2017/04/08/16/16/kicks-2213619_960_720.jpg');





​        $manager->persist($product2);

​        $manager->persist($product3);

​        $manager->persist($product4);

​        $manager->flush();

​    }

}
```



Pour créer une base de données, nous devons tout d'abord faire les commandes suivantes

- bin/console doctrine:database:create
- bin/console make:migration
- bin/console doctrine:migration:migrate



Ensuite, nous avons créé des fixtures qui permettent de créer nos jeux de données.

Nous vous avons mis un exemple au-dessus, pour faire cela nous avons utilisé la commande suivante:

- composer require orm-fixtures --dev



Puis pour les lancer :

- bin/console doctrine:fixtures:load

